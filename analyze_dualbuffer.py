#!/usr/bin/env python

import commands as CMD
import numpy as np
import sys
import math
import argparse
import csv
import pandas as pd

parser = argparse.ArgumentParser(description='NoC Traffic Analyzer')
parser.add_argument('-N', metavar='NxN', type=int, required=True, help='the size of the NoC')
parser.add_argument('-f', metavar='flowset file', type=str, default="flow.dat", help='a file containing a list of flows')
parser.add_argument('-B', metavar='burst info', type=int, default=0, help='burst, by default reads from input file')
parser.add_argument('-R', metavar='rate info', type=float, default=0.00, help='rate, by default reads from input file')
args = parser.parse_args()

# accept command-line arguments of flow set and system size
file_name = args.f
size = args.N
burst_user = args.B
rate_user = args.R

df = pd.read_csv(file_name, sep=',\s+', engine='python')

debuglog = open("log.csv","w")

w_bm = np.zeros((2*size,2*size,2*size))
w_rm = np.zeros((2*size,2*size,2*size))
pe_bm = np.zeros((2*size,2*size,2*size))
pe_rm = np.zeros((2*size,2*size,2*size))

length = len(df.index)

for x_b in range(size):
    for y in range (size):
        for x in range (size):
            #for k in range (size*size):
            for k in range (length):
                sx=int(df['sX'][k])
                sy=int(df['sY'][k])
                dx=int(df['dX'][k])
                dy=int(df['dY'][k])
                if (burst_user == 0):
                    burst = int(df['B'][k])
                else:
                    burst = burst_user
                if (rate_user == 0.00):
                    rate = float(df['R'][k])
                else:
                    rate = rate_user 
                if (sx == x and sy == y):
                    yd = size + dy
                    if (sy <= dy):
                        ys = size + sy
                    else:
                        ys = size - sy - 1
                    if (x_b == dx and x_b == sx):           # when Flows are in the right column and only moving vertically, then they dont contribute to W-FIFO size
                        pe_bm[x_b][ys,yd] = pe_bm[x_b][ys,yd] + burst
                        pe_rm[x_b][ys,yd] = pe_rm[x_b][ys,yd] + rate
                        debuglog.write("2,%d,%d,%d,%d,%d,%d,%d,%d\n" % (sx, sy, dx, dy, x_b, ys, yd,0))
                    elif (x_b == dx and x_b != sx):         # when Flows are NOT in the right column and moving horizontally, then they contribute to W-FIFO size
                        confb = 0
                        confrho_t = 0
                        for y_sb in range (size):
                            for x_sb in range (size):
                                for r in range (size*size):
                                    rsx=int(df['sX'][k])
                                    rsy=int(df['sY'][k])
                                    rdx=int(df['dX'][k])
                                    rdy=int(df['dY'][k])
                                    if (rsx == x_sb and rsy == y_sb):
                                        if (y_sb == y and ((x_sb < x and rdx > x) or (x_sb > x and rdx > x and rdx < x_sb)) and x_sb != rdx):
                                            confb = confb + burst
                                            confrho_t = confrho_t + rate
                                        break
                        confrho = 1 - confrho_t
                        firstpacket = math.ceil(confb/confrho) - 1 + math.ceil(1.0/rate)
                        net_latency = firstpacket + dx - sx
                        w_bm[x_b][ys,yd] = w_bm[x_b][ys,yd] + burst
                        w_rm[x_b][ys,yd] = w_rm[x_b][ys,yd] + rate
                        debuglog.write("1,%d,%d,%d,%d,%d,%d,%d,%d\n" % (sx, sy, dx, dy, x_b, ys, yd, net_latency))
                    break

for i in range(size):
	np.savetxt("w_rm_%s.csv" % i, w_rm[i], fmt='%.2f', delimiter=",")
	np.savetxt("w_bm_%s.csv" % i, w_bm[i], fmt='%d', delimiter=",")
	np.savetxt("pe_rm_%s.csv" % i, pe_rm[i], fmt='%.2f', delimiter=",")
	np.savetxt("pe_bm_%s.csv" % i, pe_bm[i], fmt='%d', delimiter=",")

print "Generated matrices of flows for each column in the network w_rm_*.csv, w_bm_*.csv, pe_rm_*.csv, pe_bm_*.csv" 
print "These files are needed by the next analysis step written in Matlab/Octave to compute bounds based on the set of interfering flows identified here"

debuglog.close()

print "Calling Octave script to run analysis"
CMD.getoutput('octave top_extract_all_dualbuffer.m %d' % size);

############ latency generation

data = np.genfromtxt ('log.csv', delimiter=",")
type_field = data[:,0]
x_b = data[:,5]
x = data[:,6]
y = data[:,7]
lat_1 = data[:,8]

new_len = np.shape(data)[0]

#lat_2 = np.zeros((size*size))
#total = np.zeros((size*size))
lat_2 = np.zeros((new_len))
total = np.zeros((new_len))

fread_burst = np.genfromtxt ('buffer.csv', delimiter=",")
max_fifo_size = np.atleast_1d(np.amax(fread_burst))

fread_stable = np.genfromtxt ('stable.csv', delimiter=",")
stability = np.atleast_1d(np.amin(fread_stable))

np.savetxt("buffer_max.csv", max_fifo_size, fmt='%d', delimiter=",")
np.savetxt("stable_min.csv", stability, fmt='%d', delimiter=",")


if (stability == 0):
    maximum = np.atleast_1d(0)
else:
    for i in range(new_len):
        if (type_field[i] == 1):
            d = np.genfromtxt ('inflight%d.csv' % int(x_b[i]), delimiter=",")
        elif (type_field[i] == 2):
            d = np.genfromtxt ('total%d.csv' % int(x_b[i]), delimiter=",")
        lat_2[i] = d[int(x[i]),int(y[i])]
    
    
    new_data = np.insert(data, 9, lat_2, axis=1)
    
    total = lat_1 + lat_2
    
    new_data = np.insert(new_data, 10, total, axis=1)
    np.savetxt("log_1.csv", new_data, fmt='%d', delimiter=",")
    
    maximum = np.atleast_1d(np.amax(total, axis=0))
    
np.savetxt("wctotal.csv", maximum, fmt='%d', delimiter=",")

print("Stable=%d, Max FIFO=%d, WC Latency=%d" % (stability,max_fifo_size,maximum))



