#!/usr/bin/env python

import commands as CMD
import numpy as np
import sys
import math
import argparse
import csv
import pandas as pd

parser = argparse.ArgumentParser(description='NoC Traffic Analyzer for Backpressure Torus')
parser.add_argument('-N', metavar='NxN', type=int, required=True, help='the size of the NoC')
parser.add_argument('-f', metavar='flowset file', type=str, default="flow.dat", help='a file containing a list of flows')
parser.add_argument('-B', metavar='burst info', type=int, default=0, help='burst, by default reads from input file')
parser.add_argument('-R', metavar='rate info', type=float, default=0.00, help='rate, by default reads from input file')
parser.add_argument('-M', metavar='backpressure global enable', type=int, default=1, help='backpressure enabled in each FIFO')
args = parser.parse_args()

# accept command-line arguments of flow set and system size
file_name = args.f
size = args.N
burst_user = args.B
rate_user = args.R
global_bp = args.M

df = pd.read_csv(file_name, sep=',\s+', engine='python')

pe_bm = np.zeros((size*size,size*size))
pe_rm = np.zeros((size*size,size*size))
if(global_bp):
    sw_bp = np.ones((size*size,1))
else:
    sw_bp = np.zeros((size*size,1))


length = len(df.index)

for k in range (length):
    sx=int(df['sX'][k])
    sy=int(df['sY'][k])
    dx=int(df['dX'][k])
    dy=int(df['dY'][k])
    if (burst_user == 0):
        burst = int(df['B'][k])
    else:
        burst = burst_user
    if (rate_user == 0.00):
        rate = float(df['R'][k])
    else:
        rate = rate_user 
    #pe_bm[sx*size+sy][dx*size+dy]=burst;
    #pe_rm[sx*size+sy][dx*size+dy]=rate;
    pe_bm[sy*size+sx][dy*size+dx]=burst;
    pe_rm[sy*size+sx][dy*size+dx]=rate;

np.savetxt("pe_rm.csv", pe_rm, fmt='%.2f', delimiter=",")
np.savetxt("pe_bm.csv", pe_bm, fmt='%d', delimiter=",")
np.savetxt("sw_bp.csv", sw_bp, fmt='%d', delimiter=",")

print "Generated matrices of flows for entire network pe_rm.csv, pe_bm.csv, and sw_bp.csv" 
print "These files are needed by the next analysis step written in Matlab/Octave to compute bounds based on the set of interfering flows identified here"

print "Calling Octave script to run analysis"
CMD.getoutput('octave top_extract_all_backpressure.m %d' % size);

############ latency generation

fread_burst = np.genfromtxt ('buffer.csv', delimiter=",")
max_fifo_size = np.atleast_1d(np.amax(fread_burst))

fread_total = np.genfromtxt ('total.csv', delimiter=",")
wclatency = np.atleast_1d(np.amax(fread_total))

stable = np.genfromtxt ('stable.csv', delimiter=",")

print("Stable=%d, Max FIFO=%d, WC Latency=%d" % (stable,max_fifo_size,wclatency))



