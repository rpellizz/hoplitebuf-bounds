## Copyright (C) 2018 rpellizz
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <https://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {} {@var{retval} =} west_fifo (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: rpellizz <rpellizz@DESKTOP-1A3KAMF>
## Created: 2018-09-24

function [retval B FlightLatencyW TotalLatencyS burstBack rhoBack] = west_fifo_extra_backpressure (burstW, rhoW, burstS, rhoS, burstE, rhoE, firstPacketOnly, backpressure, method)
  %WEST_FIFO_EXTRA_BACKPRESSURE Computes latencies for HopliteBuf vertical ring W -> S design with backpressure
  %
  %burstW: N x N array of burstiness of flows that arrive from W and turn S
  %rhoW: N x N array of rates of flows that arrive from W and turn S
  %burstS: N x N array of burst of flows that go from PE to S
  %rhoS: N x N array of rates of flows that go from PE to S
  %burstE: N x 1 vector of cumulative burstiness of flows that go from PE to E
  %rhoE: N x 1  vector of cumulative rates of flows that go from PE to E
  %for all inputs, row is entry, and column is exit
  %firstPacketOnly: 1 to compute injection latency for first packet only;
  %                 0 to compute injection latency for all packets in the burst
  %backpressure: N array of boolean. true means the corresponding router is backpressured
  %method: false is time-stopping method; true is backlog-bound
  %returns
  %retval: true if we have a bound; false if we have no bound
  %B: N array of max buffer size for each router
  %FlightLatencyW: N x N array of max in-flight latencies (buffer to exit) for flows from W to S that are not backpressured
  %TotalLatencyS: N x N array of total latencies (injection + in-flight) for flows from PE to S
  %burstBack: N array of burstiness of interfering flows for each backpressured router
  %rhoBack: N array of rates of interfering flows for each backpressured router
  %
  %Note: burst = 0 means that the flow does not exist. 
  %burst must otherwise be >=1
  %rate for a flow with burst = 0 MUST be 0
  %otherwise the rate must be between 0 and 1 (extremes excluded)
  
  retval = false; FlightLatencyW = 0; TotalLatencyS = 0; B = 0; burstBack = 0; rhoBack = 0;
  
   if(nargin < 4 | nargin == 5)
    'not enough input arguments'
    return
  endif
  
  [N N1] = size(burstW);
  
  if(nargin == 4)
    burstE = zeros(N, 1);
    rhoE = zeros(N, 1);
  endif
  
  if(nargin < 7)
    firstPacketOnly = 1;
  endif
  
  if(nargin < 8)
    backpressure = zeros(N, 1);
  endif
  
  if(nargin < 9)
    method = 0;
  endif
  
  [N2 N3] = size(rhoW);
  [N4 N5] = size(burstS);
  [N6 N7] = size(rhoS);
  [N8 N9] = size(burstE);
  [N10 N11] = size(rhoE);
  [N12 N13] = size(backpressure);
  if (N != N1 | N != N2 | N != N3 | N != N4 | N != N5 | N != N6 | N != N7 | N != N8 | N != N10 | N != N12 | N9 != 1 | N11 != 1 | N13 != 1)
    'sizes do not match'
    return
  endif
  
  
  %note: no check is performed to ensure that burstiness and rate values 
  %are valid to avoid slowing down the analysis
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %GENERAL STRUCTURES
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  sigmaW = burstW - rhoW;
  sigmaS = burstS - rhoS;
  
  %backpressure structures
  sigmaWB = zeros(N,N);
  sigmaWNB = zeros(N,N);
  rhoWB = zeros(N,N);
  rhoWNB = zeros(N,N);
  for i = 1:N
    if( backpressure(i) == 0)
      sigmaWNB(i,:) = sigmaW(i,:);
      rhoWNB(i,:) = rhoW(i,:);
    else
      sigmaWB(i,:) = sigmaW(i,:);
      rhoWB(i,:) = rhoW(i,:);
    endif
  endfor
  
  R = ones(N,1);
  %this term captures R(i), remaining bandwidth at node i after subtracting N->S
  for i = 1:N
	  for p = 1:i - 1
		  for l = i:N
			  R(i) = R(i) - rhoW(p,l) - rhoS(p,l);	
      endfor
		  for l = 1:p - 1
			  R(i) = R(i) - rhoW(p,l) - rhoS(p,l);
      endfor
    endfor
	  for p = i + 1:N
		  for l = i:p - 1
			  R(i) = R(i) - rhoW(p,l) - rhoS(p,l);
      endfor
    endfor
  endfor
  
  
if(method == 0)
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %TIME STOPPING BEGINS
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  %NOTE: we are trying to find a bound for flows W->S NB after leaving the buffer.
  %The flows W->S B are treated like flows PE->S for such computations.
  
  Rij = zeros (N, N);
  %this term captures R(i,j), remaining bandwidth for flow i->j
  for i = 1:N
	  for j = 1:N
		  Rij(i,j) = R(i) - sum( rhoWNB(i, :) ) + rhoWNB(i,j);
    endfor
  endfor
  
  sigma_l = zeros(N*N, 1);
  %Linearize the sigmaWNB into a vector of size N*N
  for i = 1:N
	  for j = 1:N
		  sigma_l(i*N + j - N) = sigmaWNB(i,j);
    endfor
  endfor
  
  Apartial = zeros(N,N*N);
  Extra = zeros(N, 1);
  %these two terms captures T(i), the service delay at node i. 
  %Extra(i) is the costant term depending on S flows
  %Apartial is a multiplication matrix to solve the system of sigma'
  for i = 1:N
	  for p = 1:i - 1
		  for l = i:N
			  Apartial(i,p*N + l - N) = 1 / R(i);
        Extra(i) = Extra(i) + (sigmaS(p,l)+sigmaWB(p,l)) / R(i); 
      endfor
		  for l = 1:p - 1
			  Apartial(i,p*N + l - N) = 1 / R(i);
        Extra(i) = Extra(i) + (sigmaS(p,l)+sigmaWB(p,l)) / R(i); 
      endfor
    endfor
	  for p = i + 1:N
		  for l = i:p - 1
			  Apartial(i,p*N + l - N) = 1 / R(i);
        Extra(i) = Extra(i) + (sigmaS(p,l)+sigmaWB(p,l)) / R(i); 
      endfor
    endfor
  endfor
  
  rho_AT = zeros(N*N, N*N);
  %full multiplication matrix to solve the sigma' system; 
  %note T(i) must be multiplied by rhoWNB(i,j) for each flow under analysis
  for i = 1:N
	  for j = 1:N
		  rho_AT(i*N + j - N, :) = Apartial(i, :) * rhoWNB(i,j);
    endfor
  endfor
  
  CTpartial = zeros(N, 1);
  %contains the constant terms sum_{l} sigma(i,l) / R(i) in T(i,j)
  %in other words, this are WNB flows entering at i
  %note we also sum the one under analysis; this will be taken out in CT
  for i = 1:N
	  for l = 1:N
		  CTpartial(i) = CTpartial(i)+ sigmaWNB(i,l) / R(i);
    endfor
  endfor

  CT = zeros(N*N, 1);
  rho_CT = zeros (N*N,1);
  %all constant terms that end in T(i,j)
  %we use Extra(i) + CTpartial(i), where we have to take out the f.u.a.
  %rho_CT is then scaled by rho(i,j)
  for i = 1:N
	  for j = 1:N
		  CT(i*N + j - N) = CTpartial(i) - sigmaWNB(i,j) / R(i) + Extra(i);
		  rho_CT(i*N + j - N) = rhoWNB(i,j) * CT(i*N + j - N);
    endfor
  endfor


  %can we invert? NOTE: BEWARE OF NUMERICAL ERRORS
  if (det(eye(N*N) - rho_AT) == 0)
    'singular matrix - unstable network'
    return
  endif

  %compute result for S flows in linearized array
  sigma_l_result = (eye(N*N) - rho_AT)\(sigma_l + rho_CT);

  %can't have negative burstiness, this indicates infeasibility...
  if (any(sigma_l_result < 0))
    'negative sigma - unstable network'
    return
  endif
  
  %determine Latency for west flows NB
  %delay is sigma(i,j) / R(i,j) + T(i,j) 
  %for latency add hop distance
  FlightLatencyW = zeros(N, N);
  for i = 1:N
	  for j = 1:N
      if(sigmaWNB(i,j) == 0)
        FlightLatencyW(i,j) = 0;
      elseif( rhoWNB(i,j) > Rij(i,j) )
        'exceed link utilization - unstable network'
        return
      else
        if(j >= i)
          Delta = j - i + 1;
        else
          Delta = j - i + 1 + N;
        endif
  
		    Tij = Apartial(i,:)*sigma_l_result + CT(i*N + j - N);
		    FlightLatencyW(i,j) = sigmaWNB(i,j) / Rij(i,j) + Tij + Delta;
      endif
    endfor
  endfor
  %floor the latencies 
  FlightLatencyW = floor(FlightLatencyW);
		
  %compute buffer size
  %buffer size is sum of sigma of W flows entering + sum of rho * T(i)
  B = zeros(N, 1);
  for i = 1:N
    if( sum( sigmaWNB(i, :) ) == 0)
      B(i) = 0;
    elseif( sum ( rhoWNB(i, :) ) > R(i) )
      'exceed link utilization - unstable network'
      return
    else
	    B(i) = sum( sigmaWNB(i, :) ) + ( Apartial(i,:)*sigma_l_result + Extra(i) ) * sum ( rhoWNB(i, :) );
    endif
  endfor
  %floor buffer sizes, since it has to be integer
  B = floor(B);
  
  
  %BACKPRESSURE FLOWS AND INTERFERENCE ON PE->S
  
  %backpressuring flows. These are the N->S on each backpressured router
  rhoBack = zeros(N, 1);
  burstBack = zeros(N, 1);
  for i = 1:N
    if (backpressure(i) == 0)
      continue;
    endif
    sigmaBack = 0;
	  for p = 1:i - 1
		  for l = i:N
        rhoBack(i) = 1 - R(i);
			  sigmaBack = sigmaBack + sigma_l_result(p*N + l - N) + sigmaWB(p,l) + sigmaS(p,l); 
      endfor
		  for l = 1:p - 1
			  rhoBack(i) = 1 - R(i);
			  sigmaBack = sigmaBack + sigma_l_result(p*N + l - N) + sigmaWB(p,l) + sigmaS(p,l); 
      endfor
    endfor
	  for p = i + 1:N
		  for l = i:p - 1
        rhoBack(i) = 1 - R(i);
			  sigmaBack = sigmaBack + sigma_l_result(p*N + l - N) + sigmaWB(p,l) + sigmaS(p,l); 
      endfor
    endfor
    burstBack(i) = ceil(sigmaBack + 1 + rhoBack(i));
  endfor

  %compute the interfering sigma from N->S and W->S on S port of each node i
  %Here we consider both W->S B and NB
  intersigma = zeros(N, 1);
  for i = 1:N
    %W->S
    intersigma(i) = intersigma(i) + sum( sigma_l_result(i*N + 1 - N : i*N) ) + sum(sigmaWB(i,:)); 
    %N->S
	  for p = 1:i - 1
		  for l = i:N
			  intersigma(i) = intersigma(i) + sigma_l_result(p*N + l - N) + sigmaS(p,l) + sigmaWB(p,l);	
      endfor
		  for l = 1:p - 1
			  intersigma(i) = intersigma(i) + sigma_l_result(p*N + l - N) + sigmaS(p,l) + sigmaWB(p,l);
      endfor
    endfor
	  for p = i + 1:N
		  for l = i:p - 1
			  intersigma(i) = intersigma(i) + sigma_l_result(p*N + l - N) + sigmaS(p,l) + sigmaWB(p,l);
      endfor
    endfor
  endfor
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %TIME STOPPING ENDS
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
else
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %BUFFER BOUND BEGINS
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  rhoRouter = zeros(N,1);
  sigmaRouter = zeros(N,1);
  %these terms capture sum of rho and sigma at every router, for W->S NB and N->S
  for i = 1:N
    rhoRouter(i) = sum(rhoWNB(i,:));
    sigmaRouter(i) = sum(sigmaWNB(i,:));
	  for p = 1:i - 1
		  for l = i:N
			  rhoRouter(i) = rhoRouter(i) + rhoW(p,l) + rhoS(p,l);	
        sigmaRouter(i) = sigmaRouter(i) + sigmaW(p,l) + sigmaS(p,l);	
      endfor
		  for l = 1:p - 1
			  rhoRouter(i) = rhoRouter(i) + rhoW(p,l) + rhoS(p,l);
        sigmaRouter(i) = sigmaRouter(i) + sigmaW(p,l) + sigmaS(p,l);	
      endfor
    endfor
	  for p = i + 1:N
		  for l = i:p - 1
			  rhoRouter(i) = rhoRouter(i) + rhoW(p,l) + rhoS(p,l);
        sigmaRouter(i) = sigmaRouter(i) + sigmaW(p,l) + sigmaS(p,l);	
      endfor
    endfor
  endfor
  
  %compute maximals
  rhoMax = max(rhoRouter);
  sigmaMax = max(sigmaRouter);
  sigmaTotal = sum(sum(sigmaW)) + sum(sum(sigmaS)); %sum all, NB and B
  
   %can't have rate greater than one
  if ( rhoMax >= 1)
     'exceed link utilization - unstable network'
    return
  endif
  
  %compute buffer bound
  Bbound = floor(N*N*rhoMax/(1-rhoMax)*sigmaMax + sigmaTotal);
  B = (1-backpressure)*Bbound;
  
  %determine Latency for west flows NB
  %delay is Bbound
  %for latency add hop distance
  FlightLatencyW = zeros(N, N);
  for i = 1:N
	  for j = 1:N
      if(sigmaWNB(i,j) == 0)
        FlightLatencyW(i,j) = 0;
      else
        if(j >= i)
          Delta = j - i + 1;
        else
          Delta = j - i + 1 + N;
        endif
  		    
		    FlightLatencyW(i,j) = Bbound +  Delta;
      endif
    endfor
  endfor
  %floor the latencies 
  FlightLatencyW = floor(FlightLatencyW);

  %backpressuring flows. These are the N->S on each backpressured router
  %for the NB coming either N->S, the sigma is simply the buffer size Bbound
  rhoBack = zeros(N, 1);
  burstBack = zeros(N, 1);
  for i = 1:N
    if (backpressure(i) == 0)
      continue;
    endif
    sigmaBack = 0;
	  for p = 1:i - 1
      addB = 0;
		  for l = i:N
        rhoBack(i) = 1 - R(i);
			  sigmaBack = sigmaBack + sigmaWB(p,l) + sigmaS(p,l); 
        if(sigmaWNB(p,l) > 0)
          addB = 1;
        endif
      endfor
		  for l = 1:p - 1
			  rhoBack(i) = 1 - R(i);
			  sigmaBack = sigmaBack + sigmaWB(p,l) + sigmaS(p,l); 
        if(sigmaWNB(p,l) > 0)
          addB = 1;
        endif
      endfor
      sigmaBack = sigmaBack + addB*Bbound;
    endfor
	  for p = i + 1:N
      addB = 0;
		  for l = i:p - 1
        rhoBack(i) = 1 - R(i);
			  sigmaBack = sigmaBack + sigmaWB(p,l) + sigmaS(p,l); 
        if(sigmaWNB(p,l) > 0)
          addB = 1;
        endif
      endfor
      sigmaBack = sigmaBack + addB*Bbound;
    endfor
    burstBack(i) = ceil(sigmaBack + 1 + rhoBack(i));
  endfor
  
  %compute the interfering sigma from N->S and W->S (both B and NB) on S port of each node i
  %for the NB coming either N->S or W->S, the sigma is simply the buffer size Bbound
  intersigma = zeros(N, 1);
  for i = 1:N
    %W->S
    intersigma(i) = sum(sigmaWB(i,:));
    if(any(sigmaWNB(i,:) > 0))
      intersigma(i) = intersigma(i) + Bbound;
    endif
    %N->S
	  for p = 1:i - 1
      addB = 0;
		  for l = i:N
			  intersigma(i) = intersigma(i) + sigmaWB(p,l) + sigmaS(p,l); 
        if(sigmaWNB(p,l) > 0)
          addB = 1;
        endif
      endfor
		  for l = 1:p - 1
			  intersigma(i) = intersigma(i) + sigmaWB(p,l) + sigmaS(p,l);
        if(sigmaWNB(p,l) > 0)
          addB = 1;
        endif
      endfor
      intersigma(i) = intersigma(i) + addB*Bbound;
    endfor
	  for p = i + 1:N
      addB = 0;
		  for l = i:p - 1
			  intersigma(i) = intersigma(i) + sigmaWB(p,l) + sigmaS(p,l);
        if(sigmaWNB(p,l) > 0)
          addB = 1;
        endif
      endfor
      intersigma(i) = intersigma(i) + addB*Bbound;
    endfor
  endfor
  
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %BUFFER BOUND ENDS
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

endif

  %compute the interfering rate from N->S and W->S flows (B and NB) on S port of node i
  %R is already all north, so just add W->S
  interrho = zeros(N, 1);
  for i = 1:N
      interrho(i) = R(i) - sum ( rhoW(i,:) );
  endfor

  %Latency for PE -> S
  %apply Theorem 2
  TotalLatencyS = zeros(N, N);
  for i = 1:N
    for j = 1:N
      
      if(rhoS(i,j) == 0)
        TotalLatencyS(i,j) = 0;
      else
        if(j >= i)
          Delta = j - i + 1;
        else
          Delta = j - i + 1 + N;
        endif
      
        %conflicting b. Add to intersigma the PE->E, and all other PE->S
        confb = ceil(intersigma(i) + 1 + interrho(i)) + burstE(i) + sum( burstS(i, :) ) - burstS(i,j);
        %conflicting rho. Add to interrho the PE->E, and all other PE->S
        confrho = interrho(i) - rhoE(i) - sum( rhoS(i, :) ) + rhoS(i,j);
        
        if(confrho <= 0)
          'saturated S link - unstable network'
          return
        endif
        
        if(rhoS(i,j) > confrho)
           'remaining rate less than injection rate - cannot guarantee injection'
          return
        endif       
        
        TotalLatencyS(i,j) = compute_injection_latency (burstS(i,j), rhoS(i,j), confb, confrho, firstPacketOnly) + Delta;
      endif
      
    endfor
  endfor
  
  
  retval = true;

endfunction
