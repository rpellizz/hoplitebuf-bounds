#!/usr/bin/octave -qf

arg_list = argv ();

function top_extract_all_backpressure(N)

  f1=['pe_bm.csv'];
	f2=['pe_rm.csv'];
	f3=['sw_bp.csv'];

	burst = csvread(f1);
	rate = csvread(f2);
	backpressure = csvread(f3);

	%% call analysis core function
  %% last argument:  0=time-stopping, 1=backlog
	[ret buffer total] = west_fifo_network(N, N, burst, rate, backpressure,0);
		
	%% output files
	f4 = ['buffer.csv'];
	%%f5 = ['inflight.csv'];
	f6 = ['total.csv'];
	f7 = ['stable.csv'];
	%% write out latency, fifo size information
	csvwrite(f4, buffer);
	%%csvwrite(f5, inflight);
	csvwrite(f6, total);
	csvwrite(f7, ret);

endfunction

%% TODO: How to pass a system-size argument to the Matlab/Octave script?
top_extract_all_backpressure(str2num(arg_list{1}));
