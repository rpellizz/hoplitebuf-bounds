#!/usr/bin/env python

import commands as CMD
import numpy as np
import sys
import math
import csv


data = np.genfromtxt ('buffer.csv', delimiter=",")
(xx,yy) = np.shape(data)

for x in range (xx):
    for y in range (yy):
        if (data[x][y] == 0.0):
            data[x][y] = 0
        elif ((data[x][y] > 0.0) and (data[x][y] < 33.0)):
            data[x][y] = 32     
        elif ((data[x][y] > 32.0) and (data[x][y] < 65.0)):
            data[x][y] = 64
        elif (data[x][y] > 64.0):
            data[x][y] = 128     

sum_full = np.sum(data)
maximum = np.atleast_1d(np.amax(sum_full, axis=0))
np.savetxt("buffer_add.csv", maximum, fmt='%d', delimiter=",")

