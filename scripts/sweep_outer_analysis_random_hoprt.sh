#!/bin/zsh

echo "file,x,y,seed,rate,inflight,srcq,stable,burst" > sweep_random_srcq_analysis_hoprt.csv
	
parallel --bar --gnu -j16 --header : \
	'
	files=(random_2x2 random_3x3 random_4x4 random_5x5 random_6x6 random_7x7 random_8x8 random_12x12 random_16x16)
	pex=(2 3 4 5 6 7 8 12 16 )
	pey=(2 3 4 5 6 7 8 12 16 )

	file=$files[{i}]
	x=$pex[{i}]
	y=$pey[{i}]
	 ./sweep_inner_analysis_random_hoprt.sh $x $y $file {rate} {seed} {burst}
        ext_all=`cat bin/rtflow_random_analysis_rt/test_$x\_$y\_$file\_{rate}_{seed}_{burst}/log.csv`
        
	sem --id mystr echo $file,$x,$y,{seed},{rate},$ext_all,{burst} >> sweep_random_srcq_analysis_hoprt.csv | cat'\
	::: i 1 2 3 4 \
	::: rate 5 6 8 A D 14 21 32 64 \
	::: seed `seq 0 99` \
        ::: burst 1 2 4 8 \
