#!/bin/zsh


PE_X=$1
PE_Y=$2
BENCH=$3
RATE=$4
BURST=$5

mkdir -p bin/rtflow_analysis_wsn/test_${1}_${2}_${3}_${4}_${5}
pushd bin/rtflow_analysis_wsn/test_${1}_${2}_${3}_${4}_${5}

cp ../../../../bench/$BENCH.dat .
cp ../../../../analyze_dualbuffer.py .
cp ../../../../top_extract_all_dualbuffer.m .
cp ../../../../west_fifo_extra.m .
cp ../../../../compute_injection_latency.m .

RATE_D=`echo "ibase=16;obase=A;$RATE" | bc`
RATE_F=`echo "scale=3;1/$((RATE_D))" | bc`

python analyze_dualbuffer.py -N $PE_X -f $BENCH.dat -B $BURST -R $RATE_F

pwd
popd

