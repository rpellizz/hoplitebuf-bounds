#!/bin/zsh

echo "file,x,y,rate,fullness,stable,burst,wctotal" > sweep_srcq_analysis_ws.csv
	
parallel --bar --gnu -j32 --header : \
	'
	files=(all2one_5x5 all2row_5x5 all2col_5x5)
	pex=(5 5 5 )
	pey=(5 5 5 )

	file=$files[{i}]
	x=$pex[{i}]
	y=$pey[{i}]
	 ./sweep_inner_analysis_ws.sh $x $y $file {rate} {burst}
        fullness=`cat bin/rtflow_analysis_ws/test_$x\_$y\_$file\_{rate}_{burst}/buffer_max.csv`
        stable=`cat bin/rtflow_analysis_ws/test_$x\_$y\_$file\_{rate}_{burst}/stable_min.csv`
        wctotal=`cat bin/rtflow_analysis_ws/test_$x\_$y\_$file\_{rate}_{burst}/wctotal.csv`
        
	sem --id mystr echo $file,$x,$y,{rate},$fullness,$stable,{burst},$wctotal >> sweep_srcq_analysis_ws.csv | cat'\
	::: i 1 2 3 \
	::: rate 5 6 8 A D 14 21 32 64 \
        ::: burst 1 2 4 8 \
