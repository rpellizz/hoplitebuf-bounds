## Copyright (C) 2019 rpellizz
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <https://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {} {@var{retval} =} west_fifo_network (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: rpellizz <rpellizz@DESKTOP-1A3KAMF>
## Created: 2019-05-16

function [retval B Latency] = west_fifo_network (columns, rows, burst, rho, backpressure, method)
  
  %note: indexes are c-1 + (r-1)*columns
  
  nodes = columns*rows;
  retval = 0; B = zeros(nodes, 1); Latency = zeros(nodes, nodes);
  global colrc;
  colrc = columns;
  
  if(nargin < 4)
    'not enough input arguments'
    return
  endif
  
  if(nargin < 5)
    backpressure = zeros(nodes,1);
  endif
  
  if(nargin < 6)
    method = 0;
  endif
  
  [N1 N2] = size(burst);
  [N3 N4] = size(rho);
  [N5 N6] = size(backpressure);
  
   if (N1 != nodes | N2 != nodes | N3 != nodes | N4 != nodes | N5 != nodes | N6 != 1)
    'sizes do not match'
    return
  endif
  
  %note: no check is performed to ensure that burstiness and rate values 
  %are valid to avoid slowing down the analysis
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %GET INTERFERING HORIZONTAL FLOWS FOR PE->E FLOWS
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  interrho = ones(nodes, nodes);
  interb = zeros(nodes, nodes);
  
  %rs, cs: rows and column source; rd, cd: rows and column destination; cso, cdo: column source and destination of other flow; rdo: row destination of other flow
  
  for cs = 1:columns
    for cd = 1:columns
      if(cs == cd) continue; endif %cs == cd injects south
      for rs = 1:rows   
        for rd = 1:rows
          
          if(burst(rc(rs,cs), rc(rd,cd)) == 0) continue; endif
          
          for rdo = 1:rows
          
            %add all flows that inject PE->S or PE->E at rs, cs other than the flow directed to rd, cd
            for cdo = 1:columns
              if(and(cdo == cd, rdo == rd)) continue; endif
              interrho(rc(rs, cs), rc(rd, cd)) = interrho(rc(rs, cs), rc(rd, cd)) - rho(rc(rs, cs), rc(rdo, cdo));
              interb(rc(rs, cs), rc(rd, cd)) = interb(rc(rs, cs), rc(rd, cd)) + burst(rc(rs, cs), rc(rdo, cdo));              
            endfor
            
            %add all flows on row rs that pass W->E at rs, cs
            for cso = 1:cs-1
              for cdo = cs+1:columns
                interrho(rc(rs, cs), rc(rd, cd)) = interrho(rc(rs, cs), rc(rd, cd)) - rho(rc(rs, cso), rc(rdo, cdo));
                interb(rc(rs, cs), rc(rd, cd)) = interb(rc(rs, cs), rc(rd, cd)) + burst(rc(rs, cso), rc(rdo, cdo));    
              endfor
              for cdo = 1:cso - 1
                interrho(rc(rs, cs), rc(rd, cd)) = interrho(rc(rs, cs), rc(rd, cd)) - rho(rc(rs, cso), rc(rdo, cdo));
                interb(rc(rs, cs), rc(rd, cd)) = interb(rc(rs, cs), rc(rd, cd)) + burst(rc(rs, cso), rc(rdo, cdo));  
              endfor
            endfor
            for cso = cs + 2:columns
              for cdo = cs+1:cso - 1
                interrho(rc(rs, cs), rc(rd, cd)) = interrho(rc(rs, cs), rc(rd, cd)) - rho(rc(rs, cso), rc(rdo, cdo));
                interb(rc(rs, cs), rc(rd, cd)) = interb(rc(rs, cs), rc(rd, cd)) + burst(rc(rs, cso), rc(rdo, cdo));  
              endfor
            endfor    
          
          endfor
        
        endfor
      endfor
    endfor
  endfor
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %ADD HORIZONTAL PER-HOP DELAY TO PE->E FLOWS NB, AND TOTAL PER-HOP DELAY TO B
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  for cs = 1:columns
    for cd = 1:columns
      if(cs == cd) continue; endif %cs == cd injects south
      for rs = 1:rows   
        for rd = 1:rows
          
          if(burst(rc(rs,cs), rc(rd,cd)) == 0) continue; endif
        
          %horizontal does not count turn router
          if(cd > cs)
            Latency(rc(rs,cs), rc(rd,cd)) = cd - cs;
          else
            Latency(rc(rs,cs), rc(rd,cd)) = cd - cs + columns;
          endif
          
          %for backpressured one, add vertical
          if(backpressure(rc(rs,cd)) == 1)
          if(rd > rs)
            Latency(rc(rs,cs), rc(rd,cd)) = rd - rs + 1;
          else
            Latency(rc(rs,cs), rc(rd,cd)) = rd - rs + columns + 1;
          endif
    
          endif
          
        endfor
      endfor
    endfor
  endfor
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %COMPUTE ON EACH VERTICAL LINK
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  %direct backpressure interference
  directb = zeros(rows, columns); directrho = zeros(rows, columns);
  
  %cv: the vertical column under consideration
  
  for cv = 1:columns
    %%%%%%%%%%%%
    %START VERTICAL COLUMN
    
    %compute backpressureV
    backpressureV = zeros(rows, 1);
    for r = 1:rows
      backpressureV(r) = backpressure(rc(r,cv));
    endfor
    
    %compute burstS and rhoS. These are parameters of injection PE->S at cv
    burstS = zeros(rows, rows); rhoS = zeros(rows, rows);
    for rs = 1:rows
      for rd = 1:rows
        burstS(rs, rd) = burst(rc(rs,cv), rc(rd, cv));
        rhoS(rs, rd) = rho(rc(rs,cv), rc(rd, cv));
      endfor
    endfor
    
    %compute burstW and rhoW. These are parameters of injection PE->W at cv
    burstW = zeros(rows, rows); rhoW = zeros(rows, rows);
    for cs = 1:columns
      if(cs == cv) continue; endif %cs == cv injects south
      for rs = 1:rows
        for rd = 1:rows
          burstW(rs, rd) = burstW(rs, rd) + burst(rc(rs,cs), rc(rd, cv));
          rhoW(rs, rd) = rhoW(rs, rd) + rho(rc(rs,cs), rc(rd, cv));
        endfor
      endfor
    endfor
    
    %compute burstE and rhoE. These are parameters of flows going PE->E at cv
    burstE = zeros(rows, 1); rhoE = zeros(rows, 1);
    for rs = 1:rows
      for rd = 1:rows
      
          for cd = 1:cv-1
            burstE(rs) = burstE(rs) + burst(rc(rs,cv), rc(rd,cd));
            rhoE(rs) = rhoE(rs) + rho(rc(rs,cv), rc(rd,cd));
          endfor
          for cd = cv+1:columns
            burstE(rs) = burstE(rs) + burst(rc(rs,cv), rc(rd,cd));
            rhoE(rs) = rhoE(rs) + rho(rc(rs,cv), rc(rd,cd));
          endfor
        
      endfor
    endfor
    
    %call function
    [retvalV BV FlightLatencyW TotalLatencyS burstBack rhoBack] = west_fifo_extra_backpressure (burstW, rhoW, burstS, rhoS, burstE, rhoE, 1, backpressureV, method);
    
    if(retvalV == 0)
      'failure at column analysis' 
      cv
      return;
    endif
    
    %set buffer sizes
    for r = 1:rows
      B(rc(r,cv)) = BV(r);
    endfor
    
    %set latency of PE->S nodes
    for rs = 1:rows
      for rd = 1:rows
        Latency(rc(rs,cv), rc(rd,cv)) = TotalLatencyS(rs,rd);
      endfor
    endfor
    
    %adds latency to PE->W nodes
    for cs = 1:columns
      if(cs == cv) continue; endif %cs == cv injects south
      for rs = 1:rows
        for rd = 1:rows
          
          if(burst(rc(rs,cs), rc(rd,cv)) == 0) continue; endif
          
          Latency(rc(rs,cs), rc(rd,cv)) = Latency(rc(rs,cs), rc(rd,cv)) + FlightLatencyW(rs,rd);
        endfor
      endfor
    endfor
    
    %add direct backpressure sets to any row where there is a W->S flow
    for r = 1:rows
      if(any( burstW(r, :) > 0 ))
        directb(r,cv) = burstBack(r);
        directrho(r,cv) = rhoBack(r); 
      endif
    endfor
    
    
    %END VERTICAL COLUMN
    %%%%%%%%%%%%
  endfor
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %INDIRECT BACKPRESSURE COMPUTATION
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  %construct propagation set prop
  %prop(r,c) is true if the backpressure propagates from r, c+1 back to r,c
  prop = zeros(rows, columns);
  for r = 1:rows
    for c = 1:columns
      
      %prop(r,c) should be 1 if there is any flow that crosses the W port at both r,c+1 and r,c
      anyprop = 0;
      for rd = 1:rows
        for cs = 1:c-1
          for cd = c+1:columns
            if(burst(rc(r,cs), rc(rd,cd)) > 0) anyprop = 1; endif
          endfor
          for cd = 1:cs-1
            if(burst(rc(r,cs), rc(rd,cd)) > 0) anyprop = 1; endif          
          endfor
        endfor
        for cs = c+2:columns
          for cd = c+1:cs-1
            if(burst(rc(r,cs), rc(rd,cd)) > 0) anyprop = 1; endif
          endfor
        endfor
      endfor
      prop(r,c) = anyprop;
      
    endfor
  endfor
  
  %total backpressure is sum of direct and indirect backpressure interference
  totalb = zeros(rows, columns); totalrho = zeros(rows, columns);
  for r = 1:rows
    for cd = 1:columns
      
      %add direct to total for the same column cd
      totalb(r,cd) = totalb(r,cd) + directb(r,cd); 
      totalrho(r,cd) = totalrho(r,cd) + directrho(r,cd);
      
      %iterate backward from cd, covering all other columns until it stops
      stopadd = false;
      for c = cd - 1: -1:1
        if(prop(r,c))
          totalb(r,c) = totalb(r,c) + directb(r,cd); 
          totalrho(r,c) = totalrho(r,c) + directrho(r,cd);
        else
          stopadd = true;
          break;
        endif 
      endfor
      if(stopadd) continue; endif
      for c = columns: -1 : cd+1
        if(prop(r,c))
          totalb(r,c) = totalb(r,c) + directb(r,cd); 
          totalrho(r,c) = totalrho(r,c) + directrho(r,cd);
        else
          break;
        endif
      endfor
    
    endfor
  endfor
  
  %add backpressure interference rs,cs+1 to any P->E flows at rs,cs
  for cs = 1:columns
    for cd = 1:columns
      if(cs == cd) continue; endif %cs == cd injects south
      for rs = 1:rows   
        for rd = 1:rows
          
          if(burst(rc(rs,cs), rc(rd,cd)) == 0) continue; endif
          
          interrho(rc(rs,cs), rc(rd,cd)) = interrho(rc(rs,cs), rc(rd,cd)) - totalrho(rs,1+mod(cs, columns));
          interb(rc(rs,cs), rc(rd,cd)) = interb(rc(rs,cs), rc(rd,cd)) + totalb(rs,1+mod(cs,columns));
 
        endfor
      endfor
    endfor
  endfor
  
        
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %COMPUTE INJECTION LATENCY PE->E
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  for cs = 1:columns
    for cd = 1:columns
      if(cs == cd) continue; endif %cs == cd injects south
      for rs = 1:rows   
        for rd = 1:rows
          
          if(burst(rc(rs,cs), rc(rd,cd)) == 0) continue; endif     
          
          if(interrho(rc(rs,cs), rc(rd,cd)) <= 0)
            'saturated S link - unstable network at'
            [rs cs rd cd]
            return
          endif
          
          if(rho(rc(rs,cs), rc(rd,cd)) > interrho(rc(rs,cs), rc(rd,cd)))
            'remaining rate less than injection rate - cannot guarantee injection at'
            [rs cs rd cd]
            return
          endif
          
          Latency(rc(rs,cs), rc(rd,cd)) = Latency(rc(rs,cs), rc(rd,cd)) + compute_injection_latency_bp (burst(rc(rs,cs), rc(rd,cd)), rho(rc(rs,cs), rc(rd,cd)), interb(rc(rs,cs), rc(rd,cd)), interrho(rc(rs,cs), rc(rd,cd)), 1);     
        
        endfor
      endfor
    endfor
  endfor
  
endfunction


function index = rc(r, c)
  
  global colrc;
  
  index = c + (r - 1)*colrc;

endfunction

