#! /usr/bin/octave -qf

arg_list = argv ();

function top_extract_all(N)

	retM=zeros(N,N);
	BM=zeros(N,N);

	for i = 0:N-1
		%% input files
		filename1 = ['w_bm_',num2str(i),'.csv'];
		filename2 = ['w_rm_',num2str(i),'.csv'];
		filename3 = ['pe_bm_',num2str(i),'.csv'];
		filename4 = ['pe_rm_',num2str(i),'.csv'];
	        filename6 = ['inflight',num2str(i),'.csv'];
	        filename7 = ['total',num2str(i),'.csv'];
		burstW = csvread(filename1);
		rhoW = csvread(filename2);
		burstS = csvread(filename3);
		rhoS = csvread(filename4);

		%% call analysis core function
		[ret B flight total] = west_fifo_extra(burstW, rhoW, burstS, rhoS);
		
		retM(:,i+1)=ret;
		BM(:,i+1)=B;

		%% TODO: Understand how latency information gets added up
	        csvwrite(filename6, flight);
	        csvwrite(filename7, total);
	end

	%% output files
	filename5 = ['buffer.csv'];
	filename8 = ['stable.csv'];
	%% write out burst, latency, fifo size information
	csvwrite(filename5, BM);
	csvwrite(filename8, retM);
endfunction

%% TODO: How to pass a system-size argument to the Matlab/Octave script?
top_extract_all(str2num(arg_list{1}));
